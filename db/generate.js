const { date } = require('faker')

module.exports = () => {
  const faker = require('faker')
  const _ = require('lodash')


  function getRandomImage() {
    const baseUrl = "https://source.unsplash.com/random?sig="
    const imageUrl = baseUrl + _.random(1,100)
    return imageUrl
  }
  function gen_commets() {
    let comments = []
    let commet_number = _.random(3,5)
    for (let i = 0; i < commet_number; i++) {
      let comment = {
        userName: faker.name.firstName(),
        userAvatar: faker.image.avatar(),
        comment: faker.lorem.sentences(),
        commentDate: faker.datatype.datetime(2020),
      }
      comments.push(comment)
    }
    return comments
  }
  function gem_banners() {
    let banners_img_url = []
    for (let i = 0; i < 5; i++) {
      let tags = ['product', 'sneaker', 'cool', 'shop', 'item']
      let baseUrl = "https://source.unsplash.com/random/750x390?"
      let imageUrl = baseUrl + tags[i]
      let obj = {
        imageUrl
      }
      banners_img_url.push(obj)
    }
    return banners_img_url
  }
  function gen_goods(item_times, n, offset) {
    let items = []
    topImages = []
    for (let i = 0; i < 5; i++) {
      topImages.push(getRandomImage())
    }
    for (let i = 1000; i < 1000+item_times; i++) {
      let item = {
        id: i + n*20 + offset*200,
        // id2: faker.datatype.string(),
        price: faker.commerce.price(),
        discount: 0.9, // 10% off
        product: faker.commerce.product(),
        department: faker.commerce.department(),
        name: faker.commerce.productName(),
        adjective: faker.commerce.productAdjective(),
        material: faker.commerce.productMaterial(),
        description: faker.commerce.productDescription(),
        sell: faker.datatype.number(5000),
        collect: faker.datatype.number(5000),
        views: faker.datatype.number(5000, 20000),
        imageUrl: getRandomImage(),
        topImages: topImages,
        shopName: faker.company.companyName(),
        shopSells: _.random(10000, 99999),
        shopLogo: faker.image.avatar(),
        shopScore: [4.2, 5, 3],
        // shopTag: [],
        shopDesc: faker.lorem.paragraph(),
        goodCount: faker.datatype.number(100, 500),
        comments: gen_commets(),
      }
      items.push(item)
    }
    return items
  }
  return {
    // goods: _.times(19, (n) => {
    //   return {[
    //     'news': faker.commerce.productName(),
    //   ]
    //     title: faker.commerce.productName(),
    //   }
    // })
    banners: {
      banners: gem_banners(),
    },
    goods: {
      'pop': _.times(10, (n) => { // id從1000-1199
        return {
          page: n,
          list: function(item_times, n) {
                  return gen_goods(item_times, n, 0)
                }(20, n)
        }
        n++
      }),
      'new': _.times(10, (n) => { // id從1200-1399
        return {
          page: n,
          list: function(item_times, n) {
            return gen_goods(item_times, n, 1)
          }(20, n)
        }
        n++
      }),
      'sell': _.times(10, (n) => { // id從1400-1599
        return {
          page: n,
          list: function(item_times, n) {
            return gen_goods(item_times, n, 2)
          }(20, n)
        }
        n++
      }),
    }
  }
}

// // index.js
// module.exports = () => {
//   const faker = require('faker')
//   const _ = require('lodash')

//   const data = {
//     homeData: {
//       data: [{
//         hot: [],
//         new: [],
//         sell: []
//       }]
//     }
//   }
//   // Create 1000 users
//   for (let i = 0; i < 1000; i++) {
//     homeData.data.hot.push({ id: i, 
//                     color: faker.commerce.color(),
//                     department: faker.commerce.department(),
//                     productName: faker.commerce.productName(),
//                     price: faker.commerce.price(),
//                     productAdjective: faker.commerce.productAdjective(),
//                     productMaterial: faker.commerce.productMaterial(),
//                     product: faker.commerce.product(),
//                     productDescription: faker.commerce.productDescription(),

//     })
    

//   }
//   return data
// }