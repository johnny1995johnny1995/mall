import {request} from './request'


// 在這邊封裝一層 統一管理home page 所需要的request
// 之後import getHomeMultidata 在直接呼叫就可以了
// home.vue不需要去管url 參數之類的 ，而是這一層需要設定
export function getHomeMultidata() { 
  return request({ // return request 返回的資料
    url: '/banners',
  })
}

export function getHomeGoods(type, page) {
  return request({
    url: '/home/data',
    params: {
      type, 
      page
    }
  })
}