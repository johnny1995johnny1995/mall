import axios from 'axios'

// ES6 Promise的封裝
export function request(options) {
  return new Promise((resolve, reject) => {
    // 1.創建axios instance
    const instance = axios.create({
      baseURL: 'http://localhost:3000',
      timeout: 10000
    })

    // interceptors
    instance.interceptors.response.use(res => {
      return res.data
    })

    // 封裝
    instance(options)
        .then(res => {
          resolve(res)
        }).catch(err => {
          reject(err)
    })
  })
}
