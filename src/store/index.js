import Vue from 'vue'
import Vuex from 'vuex'

import mutations from './mutations'
import actions from './actions'
import state from './state'

import getters from './getters'


Vue.use(Vuex)

const store = new Vuex.Store({
  state,
  mutations, // def: 盡量只做修改state中的內容，若有複雜判斷條件建議放在action
  actions,
  getters,
})

export default store