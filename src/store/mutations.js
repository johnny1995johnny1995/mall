export default {
  addCounter(state, payload) { // 若有 把count+1
    payload.count += 1
  },
  addToCart(state, payload) { // 若沒有 將它加到購物車
    payload.checked = true
    state.cartList.push(payload) 
  }
}