export default {
  addCart(context, payload) {
    return new Promise((resolve, reject) => {
      let oldProduct = null
    
      for(let item of context.state.cartList){
        if (item.id == payload.id) { // 若cartlist 裡的item id 和 傳進來的一樣，表示已經有了
          // console.log('same');
          oldProduct = item 
        }
      }
      
      if (oldProduct) { // 已在cartlist裡面
        // oldProduct.count += 1
        context.commit('addCounter', oldProduct)
        resolve('已將購物車中此商品數量再+1')
      } else {          // 沒有在cartlist
        payload.count = 1
        context.commit('addToCart', payload)
        // context.state.cartList.push(payload)
        resolve('已將商品加入購物車')
      }
    })
  }
}